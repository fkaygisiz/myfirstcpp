#ifndef OGRENCI_H
#define OGRENCI_H
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Ogrenci
{
public:
    Ogrenci *sonra = NULL;
    Ogrenci()
    {
    }
    Ogrenci(int ono, string oad, string osoyad, vector<string> obolum)
    {
        ogrenci_no= ono;
        ad =oad;
        soyad = osoyad;
        bolum= obolum;
    }
    void yazdir()
    {
        std::string s;
        for (const auto &piece : bolum) s += ", " + piece;
        string formattedS = s.substr(2, s.length());
        cout << ogrenci_no << "\t" << ad << "\t" <<soyad << "\t" << formattedS << endl;
    }

    void yazdirBolumsuz()
    {
        cout << ogrenci_no << "\t" << ad << "\t" <<soyad << endl;
    }

    void baslikYazdir()
    {
        cout << "no\tad\tsoyad\t bolum" << endl;
    }
    void setOgrenci_no(int no)
    {
        ogrenci_no = no;
    }
    int getOgrenci_no()
    {
        return ogrenci_no;
    }

    void setAd(string a)
    {
        ad = a;
    }
    void setSoyad(string s)
    {
        soyad = s;
    }
    void setBolum(vector<string> b)
    {
        bolum = b;
    }
    string getAd()
    {
        return ad;
    }
    string getSoyad()
    {
        return soyad;
    }
    vector<string> getBolum()
    {
        return bolum;
    }

    Ogrenci * clone () const        // Virtual constructor (copying)
    {
        return new Ogrenci (*this);
    }

protected:

private:
    int ogrenci_no;

    string ad;
    string soyad;
    vector<string> bolum;
};

#endif // OGRENCI_H
