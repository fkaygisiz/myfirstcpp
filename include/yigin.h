#ifndef YIGIN_H
#define YIGIN_H
#include "Ogrenci.h"
#include "OgrenciOlustur.h"
#include <iostream>

class Yigin
{
public:
    Yigin()
    {
        //ctor
    }
    Ogrenci* enUst = NULL;
    void push(Ogrenci* ogr)
    {
        ogr->sonra = enUst;
        enUst = ogr;
    }
    Ogrenci* getEnUst()
    {
        return enUst;
    }
    Ogrenci* pop()
    {
        Ogrenci* popOgrenci;
        if(enUst==NULL)
        {

            cout<<"Yigin bos"<<endl;
            popOgrenci = NULL;
        }
        else
        {
            cout<<"liste ustunden getirilen ogrenci "<< enUst->getOgrenci_no() <<endl;
            popOgrenci = enUst;
            enUst = enUst->sonra;
        }
        return popOgrenci;
    }
    void listele()
    {
        Ogrenci* ptr;
        if(enUst==NULL)
            cout<<"yigin bos";
        else
        {
            ptr = enUst;
            cout<<"Yigin elemanlari : "<<endl;
            ptr->baslikYazdir();
            while (ptr != NULL)
            {
                ptr->yazdir();
                ptr = ptr->sonra;
            }
        }
        cout<<endl;
    }

    Ogrenci* bul(int no)
    {
        Ogrenci* ptr;
        if(enUst==NULL)
            return NULL;
        else
        {
            ptr = enUst;
            while (ptr != NULL)
            {
                if(ptr->getOgrenci_no() == no)
                {
                    return ptr;
                }
                ptr = ptr->sonra;
            }
        }
        return NULL;
    }

    Yigin farkliSoyadlidaslariBul()
    {
        Yigin adaslar;
        Ogrenci* ptr;
        if(enUst==NULL)
            return adaslar;
        else
        {
            OgrenciOlustur oo;
            ptr = enUst;
            while (ptr != NULL)
            {
                if(farkliSoyadlidaslarVarMi(ptr->getAd(), ptr->getSoyad()))
                {
                    adaslar.push(oo.klonla(ptr));
                }
                ptr = ptr->sonra;
            }
        }
        return adaslar;
    }

    bool farkliSoyadlidaslarVarMi(string isim1, string soyad1)
    {
        Ogrenci* ptr;
        if(enUst==NULL)
            return false;
        else
        {
            ptr = enUst;
            while (ptr != NULL)
            {
                if(ptr->getAd() == isim1 && soyad1 != ptr->getSoyad())
                {
                    return true;
                }
                ptr = ptr->sonra;
            }
        }
        return false;
    }

protected:

private:
};

#endif // YIGIN_H
