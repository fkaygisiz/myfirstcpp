#ifndef KUYRUK_H
#define KUYRUK_H
#include "Ogrenci.h"
#include "OgrenciOlustur.h"
#include <iostream>

class Kuyruk
{
public:


    Ogrenci *on = NULL;
    Ogrenci *arka = NULL;
    Ogrenci *gecici;
    Kuyruk() {}
    ~Kuyruk() {}

    Ogrenci* getOn()
    {
        return on;
    }
    void Ekle(Ogrenci* dugum1)
    {
        if (arka == NULL)
        {
            arka = dugum1;
            on = arka;
        }
        else
        {
            gecici = dugum1;
            arka->sonra = gecici;
            arka = gecici;
        }
    }
    void Sil()
    {
        gecici = on;
        if (on == NULL)
        {
            cout << "Liste Bostu" << endl;
            return;
        }
        else if (gecici->sonra != NULL)
        {
            gecici = gecici->sonra;
            cout << "Listeden silindi : " << on->getOgrenci_no() << endl;
            free(on);
            on = gecici;
        }
        else
        {
            cout << "Listeden silindi : " << on->getOgrenci_no() << endl;
            free(on);
            on = NULL;
            arka = NULL;
        }
    }
    void Listele()
    {
        gecici = on;
        if ((on == NULL) && (arka == NULL))
        {
            cout << "Liste Bos" << endl;
            return;
        }
        else
        {

            cout << "Ogrenci Listesi: " << endl;
            gecici->baslikYazdir();
            while (gecici != NULL)
            {
                gecici->yazdir();
                gecici = gecici->sonra;
            }
            cout << endl;
        }
    }
    Ogrenci* bul(int no)
    {
        gecici = on;
        if ((on == NULL) && (arka == NULL))
        {
            return NULL;
        }
        else
        {
            while (gecici != NULL)
            {
                if(gecici->getOgrenci_no() == no)
                {
                    return gecici;
                }
                gecici = gecici->sonra;
            }
            cout << endl;
        }
        return NULL;
    }
protected:

private:
};

#endif // KUYRUK_H
