#include "Yigin.h"
#include "Ogrenci.h"
#include "OgrenciOlustur.h"
#include "Kuyruk.h"
#include <iostream>
using namespace std;

const string veri_yapilari_dersi = "Veri Yapilari";
const string matematik_dersi = "Matematik";

const string bilgisayar_muh = "Bilgisayar Muhendisligi";
const string elektronik_muh = "Elektronik Muhendisligi";

OgrenciOlustur doL;
Yigin veriYapOgrencileri;
Kuyruk matematikOgrencileri;
Yigin veriYapOgrencileriAGrup;
Yigin veriYapOgrencileriBGrup;
void veriYapOgrencileriDoldur()
{
    veriYapOgrencileri.push(new Ogrenci(1, "ali", "ada", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(2, "ali", "adak", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(3, "cem", "kaya", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(4, "fatih", "demir", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(5, "sila", "meric", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(6, "sila", "ada", {bilgisayar_muh}));
    veriYapOgrencileri.push(new Ogrenci(7, "ali", "arda", {bilgisayar_muh}));
    veriYapOgrencileri.listele();
}

void matematikOgrencileriDoldur()
{
    matematikOgrencileri.Ekle(new Ogrenci(100, "mehmet", "adali", {bilgisayar_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(2, "ali", "adak", {bilgisayar_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(3, "cem", "kaya", {bilgisayar_muh, elektronik_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(400, "hasan", "guzel", {elektronik_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(500, "aysima", "kayaci", {elektronik_muh, bilgisayar_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(600, "demet", "koc", {bilgisayar_muh}));
    matematikOgrencileri.Ekle(new Ogrenci(7, "ali", "arda", {bilgisayar_muh}));
    matematikOgrencileri.Listele();
}

void derslereManuelOgrenciEkle()
{
    int tercih;

    do
    {
        cout<<"Hangi Ders"<<endl;
        cout<<"1) "<< veri_yapilari_dersi <<endl;
        cout<<"2) "<< matematik_dersi <<endl;
        cout<<"3)  Manuel ekleme cikis"<<endl;
        cin>> tercih;
        switch(tercih)
        {
        case 1:
        {

            Ogrenci* dugum1 = NULL;
            dugum1 = doL.olustur();
            dugum1->yazdir();
            veriYapOgrencileri.push(dugum1);
            break;
        }
        case 2:
        {
            Ogrenci* dugum1 = NULL;
            dugum1 = doL.olustur();
            dugum1->yazdir();
            matematikOgrencileri.Ekle(dugum1);
            break;
        }
        case 3:
        {
            break;
        }
        default:
        {
            cout<<"Gecersiz tercih"<<endl;
        }
        }
    }
    while(tercih!=3);
}

Yigin her2DersiAlanlariListele()
{
    Yigin ortak;
    cout<<"****************"<<endl;
    cout<<"Her 2 dersi Alanlar�n Listesi : "<<endl;
    Ogrenci* o1 = veriYapOgrencileri.getEnUst();
    Ogrenci* o = o1->clone();
    do
    {
        Ogrenci* m = matematikOgrencileri.bul(o->getOgrenci_no());
        if(m != NULL)
        {
            ortak.push(doL.klonla(o));
            // m->yazdirBolumsuz();
        }
        o = o->sonra;
    }
    while(o != NULL);
    ortak.listele();
    cout<<"****************"<<endl;
    return ortak;
}

void sadeceMatematikAlanlariListele()
{
    cout<<"****************"<<endl;
    cout<<"Sadece Matematik Dersi Alanlarin Listesi : "<<endl;
    Ogrenci* o = matematikOgrencileri.getOn();
    do
    {
        Ogrenci* m = veriYapOgrencileri.bul(o->getOgrenci_no());
        if(m == NULL)
        {
            o->yazdirBolumsuz();
        }
        o = o->sonra;
    }
    while(o != NULL);
    cout<<"****************"<<endl;
}

void veriYapilariABGrup()
{


    Ogrenci* o = veriYapOgrencileri.getEnUst();
    do
    {
        int no = o->getOgrenci_no();
        if(no%2 == 0)
        {
            veriYapOgrencileriAGrup.push(doL.klonla(o));
        }
        else
        {
            veriYapOgrencileriBGrup.push(doL.klonla(o));
        }

        o = o->sonra;
    }
    while(o!= NULL);
    cout<<"A grubu "<<endl;
    veriYapOgrencileriAGrup.listele();
    cout<<"B grubu "<<endl;
    veriYapOgrencileriBGrup.listele();

}

void adaslariBul()
{
    Yigin ortakIsimliler;
    Yigin ortaklar = her2DersiAlanlariListele();
    Yigin adaslar = ortaklar.farkliSoyadlidaslariBul();
    cout<<"Adas Listesi "<<endl;
    adaslar.listele();
}

void dersiAlanlariListele()
{

    cout<<"Veri Yapilari Ogrencileri "<<endl;
    veriYapOgrencileri.listele();
    cout<<"MatematikOgrencileri "<<endl;
    matematikOgrencileri.Listele();
}

bool bilgisayarMuhendisligi(vector<string> bolumler){
    for(string bolum : bolumler) {
        if(bolum.find(bilgisayar_muh)>=0 ){
            return true;
        }
    }
    return false;
}

void bilgisayarMuhendisleriniBul()
{
    Yigin yigin;
    Ogrenci* omat = matematikOgrencileri.getOn();
    Ogrenci* o = omat->clone();
    do{
        if(bilgisayarMuhendisligi(o->getBolum())){
            yigin.push(doL.klonla(o));
        }
        o= o->sonra;
    }while(o != NULL);

    Ogrenci* over = veriYapOgrencileri.getEnUst();
    Ogrenci* o1 = over->clone();
    do{
        if(bilgisayarMuhendisligi(o1->getBolum()) && yigin.bul(o1->getOgrenci_no())==NULL ){
            yigin.push(doL.klonla(o1));
        }
        o1= o1->sonra;
    }while(o1 != NULL);
    cout<<"bilgisayarMuhendisleriniBul sonuc"<<endl;
    yigin.listele();
}

void islemYap()
{
    int tercih;
    do
    {
        cout<<"0) Dersleri alanlari listele"<<endl;
        cout<<"1) her 2 dersi alanlari listele"<<endl;
        cout<<"2) sadece mat alanlar� listele"<<endl;
        cout<<"3) veri yapilari A B grup"<<endl;
        cout<<"4) veri yapilari ya da matematik alan Bilg Muh. ogrencilerini sec"<<endl;
        cout<<"5) veri yapilari ve matemeatik alan adaslar"<<endl;
        cout<<"6) Ana Menuye Don"<<endl;
        cin>>tercih;

        switch(tercih)
        {
        case 0:
        {

            dersiAlanlariListele();
            break;
        }
        case 1:
        {

            her2DersiAlanlariListele();
            break;
        }
        case 2:
        {
            sadeceMatematikAlanlariListele();
            break;
        }
        case 3:
        {
            veriYapilariABGrup();
            break;
        }
        case 4:
        {
            bilgisayarMuhendisleriniBul();
            break;
        }
        case  5:
        {
            adaslariBul();
            break;
        }
        case 6:
        {
            cout<<"Cikis"<<endl;
            break;
        }
        default:
        {
            cout<<"Gecersiz tercih"<<endl;
        }
        }
    }
    while(tercih!=6);
}
int main()
{
    int tercih;



    do
    {
        cout<<"1) Derslere statik eleman ekle"<<endl;
        cout<<"2) Derslere manuel eleman ekle"<<endl;
        cout<<"3) Islem Yap"<<endl;
        cout<<"4) Cikis"<<endl;
        cout<<"Tercihinizi Girin "<<endl;
        cin>>tercih;

        switch(tercih)
        {
        case 1:
        {

            veriYapOgrencileriDoldur();
            matematikOgrencileriDoldur();
            break;
        }
        case 2:
        {
            derslereManuelOgrenciEkle();
            break;
        }
        case 3:
        {
            islemYap();
            break;
        }
        case 4:
        {
            cout<<"Cikis"<<endl;
            break;
        }
        default:
        {
            cout<<"Gecersiz tercih"<<endl;
        }
        }
    }
    while(tercih!=4);
    return 0;
}
